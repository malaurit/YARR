###This file will step the bias voltage on a keithley2400 and perform threshold scans (differential,linear, and syncronous).

from pymeasure.instruments.keithley import Keithley2400
from numpy import mean
from numpy import arange
import sys
from time import sleep
from timeit import default_timer as timer
import os
import subprocess

ADDRESS = 24 #Sourcemeter address40
COMPLIANCE_CURR = 0.000020 #Compliance current
AVERAGES = 5
nplc = 1 #Minimum 0.01, maximum 10
#VOLTAGES = [0,0.01, 0.012585, 0.015850, 0.019955, 0.025120, 0.031620, 0.039810, 0.050120, 0.063095, 0.079430, 0.1, 0.12589, 0.158490, 0.199545, 0.25120, 0.31620, 0.39810, 0.50120,0.63095, 0.79430, 1, 1.25890, 1.5850, 1.9955]
VOLTAGES = arange(0,0.8,0.05)

#Open file where results will be saved
__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
f = open(os.path.join(__location__, 'CurrentVsbias.csv'), 'w')

print("## Attempting to connect to Keithley2400 on GPIB address " + str(ADDRESS) + " ##")
k = Keithley2400("GPIB::24")
print("## Asking for IDN. If this takes longer than a second, something is wrong. ##")
idn = k.ask("*IDN?")

if idn == '':
  print("Bad IDN. Does the instrument really have the GPIB address " + str(ADDRESS) + "?")
  sys.exit()
else:
  print("Connection success. Instrument ID:\n" + idn)
  
  
print("## Configuring Keithley 2400 sourcemeter ##")
print("Resetting instrument.")
k.reset()

print("\nConfiguring as voltage source.\n\t- Compliance current: " + str(COMPLIANCE_CURR))
k.use_front_terminals()
k.apply_voltage(compliance_current=COMPLIANCE_CURR)

print("\nConfiguring for current measurement.")
print("\t- Maximum current: " + str(COMPLIANCE_CURR*1.1) + " A.")
print("\t- NPLC rate: " + str(nplc))
k.measure_current(nplc = nplc, current=COMPLIANCE_CURR*1.1)
sleep(0.1)
print("\t- Points per measurement: " + str(AVERAGES))
k.config_buffer(points=AVERAGES)

print("\nSetting source voltage to 0V.")
k.source_voltage = 0

print("\nEnabling 4-wire measurement.")
k.wires= 4

print("\nEnabling output.")
k.enable_source()


for voltage in VOLTAGES:
  print("Ramping to " + str(voltage) + " V")
  k.ramp_to_voltage(voltage)
  sleep(0.5)
  c = mean(k.current)
  print("Current: " + str(c))
  f.write(str(voltage) + "," + str(c) + "\n")
  #Launch scan
  print("Launching scan.\n")
  cmd = "bash /home/rd53/Documents/RD53A/yarr-fork/src/bash/diffandlinandsync_thresholdscan.sh"
  handle = subprocess.Popen("xterm -e \""+cmd+"; sleep 2 && exit\"", shell=True)
  handle.communicate()
f.close()